{#
/**
 * @file
 * Default theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   "/" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Navigation:
 * - breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title: The page title, for use in the actual content.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 * - messages: Status and error messages. Should be displayed prominently.
 * - tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - action_links: Actions local to the page, such as "Add menu" on the menu
 *   administration interface.
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.navigation: Items for the navigation region.
 * - page.navigation_collapsible: Items for the navigation (collapsible) region.
 * - page.highlighted: Items for the highlighted content region.
 * - page.help: Dynamic help text, mostly for admin pages.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.footer: Items for the footer region.
 *
 * @ingroup templates
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}
{% include '@apt_theme/partials/navigation.html.twig' %}

{% include '@apt_theme/partials/header.html.twig' %}


<main id="what-is-ap-page" role="main" class="container-fluid">
    <div class="row what-is-ap">
        <div class="col-sm-8 definition">
            <h3>Effective adaptation planning means understanding changes and responding to them.</h3>
            <p>Adaptation planning is a process of adjustment to the impacts of climate change, including actions taken to reduce the negative impacts of climate change, or to take advantage of emerging opportunities.</p>
            <h3>Good adaptation planning should lead to meaningful action.</h3>
            <p>As our knowledge of climate change improves we can pursue stronger adaptation. Adaptation planning should be based on evidence which can then inform a set of priorities and actions. This evidence might relate to climate change impacts, vulnerabilities and risks but should also account for the needs of different stakeholders, and the capacity of those who will implement the plan.</p>
            <h3>Strong adaptation plans are responsive to national and regional context.</h3>
            <p>Good adaptation planning provides a platform for implementation by ensuring that activities fit within a broader set of established objectives. In the Pacific, National Adaptation Plans and Joint National Action Plans (NAPs/JNAPs) are critical in framing each countries adaptation priorities. There are a wide range of resources to support the NAP development process including through the NAP Global Network.</p>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#resources-modal">Resources to Support NAP Development</button>
        </div>
        <div class="col-sm-4 diagram">
            <img src="/themes/APT/images/apt_diagram.png" class="img-responsive" alt="APT Diagram" />
        </div>
    </div>
    <div class="row principles-heading">
        <div class="col-sm-12">
            <h2>Principles of Good Adaptation Planning</h2>
        </div>
    </div>
    <div class="row principles">
        <div class="col-sm-4 understand-it">
            <h3>Understand It</h3>
            <ul>
                <li>Risk management focuses on identifying and assessing risks, and managing those risks to minimise impact.</li>
                <li>Risk is a combination of a the nature of the hazard, likelihood of exposure, and vulnerability of who or what is being impacted.</li>
                <li>Perception of risk is subjective, and climate risk is linked to environmental, social and economic context.</li>
                <li>Different communities, ecosystems and sectors are affected differently by climate change. The same climate change impacts may affect communities, ecosystems and sectors very differently.</li>
                <li>It is important to ensure that planning considers future climate change impacts, as well as those already being experienced.</li>
            </ul>
            <h4><i class="fa fa-book"></i> Resources on Understanding Climate Risk</h4>
            <div class="list-group resources">
              <a href="https://pace.usp.ac.fj/11643-2/" class="list-group-item">
                <h4 class="list-group-item-heading">Dumaru, P., Martin, T. K., Lowry, B., Manuella, T., Deiye, T., Koppert T., Arudovo, W., Fakaosi, T., Powell, T., Vosa, W., Southcombe, D. And Holland, E. (in press). <em>Pacific Islands Community Vulnerability Assessment (CIVA): A Tool for Resilience Management.</em> Pacific Centre for Environment and Sustainable Development. The University of the South Pacific, Suva, Fiji.</h4>
                <p class="list-group-item-text">https://pace.usp.ac.fj/11643-2/</p>
              </a>
              <a href="https://www.adaptationcommunity.net/?wpfb_dl=203" class="list-group-item">
                <h4 class="list-group-item-heading">GIZ (2017) <em>The Vulnerability Sourcebook Concept and guidelines for standardised vulnerability assessments.</em></h4>
                <p class="list-group-item-text">https://www.adaptationcommunity.net/?wpfb_dl=203</p>
              </a>
            </div>
        </div>
        <div class="col-sm-4 take-action">
            <h3>Take Action</h3>
            <ul>
                <li>Prioritise risk and focus on actions to manage priority climate risks (i.e. impacts being experienced now vs. issues requiring long term planning).</li>
                <li>In planning for adaptation, ensure you work in partnership with all relevant stakeholders, to promote ownership and transparency.</li>
                <li>Consider different types adaptation responses i.e. soft, hard or ecosystem-based.</li>
                <li>Understand that some adaptation options may bring benefits, but also disadvantages – strong planning can help minimise these disadvantages and help to communicate how actions have been prioritised.</li>
                <li>Uncertainty is a part of adaptation planning - we cannot know exactly what will happen, but we can plan for events that we are more certain about.</li>
                <li>Avoid actions that prevent or limit future adaptation options or restrict the ability of others to adapt.</li>
            </ul>
            <h4><i class="fa fa-book"></i> Resources on Planning Adaptation Actions</h4>
            <div class="list-group resources">
              <a href="http://www.pacificclimatechange.net/sites/default/files/documents/FRDP_2016_Resilient_Dev_pacific.pdf" class="list-group-item">
                <h4 class="list-group-item-heading">Pacific Community, Secretariat of the Pacific Regional Environment Programme, Pacific Islands Forum Secretariat, United Nations Development Programme, United Nations Office for Disaster Risk Reduction and University of the South Pacific (2016) <em>Framework for Resilient Development in the Pacific: An Integrated Approach to Address Climate Change and Disaster Risk Management 2017 – 2030.</em></h4>
                <p class="list-group-item-text">http://www.pacificclimatechange.net/sites/default/files/documents/FRDP_2016_Resilient_Dev_pacific.pdf</p>
              </a>
              <a href="https://cdkn.org/wp-content/uploads/2012/10/CDKN-CCD-Planning_english.pdf" class="list-group-item">
                <h4 class="list-group-item-heading">Mitchell, T. & Maxwell, S. Climate & Development Knowledge Network (2010). <em>Defining climate compatible development.</em></h4>
                <p class="list-group-item-text">https://cdkn.org/wp-content/uploads/2012/10/CDKN-CCD-Planning_english.pdf</p>
              </a>
            </div>
        </div>
        <div class="col-sm-4 be-flexible">
            <h3>Be Flexible</h3>
            <ul>
                <li>Use adaptive management to cope with uncertainty. A 'phased' approach in steps will build flexibility and resilience in your planning.</li>
                <li>Make effective use of monitoring and evaluation data when planning to ensure your ability to respond to new information about climate change impacts and changing vulnerabilities and risks.</li>
                <li>Frame and communicate specific, measurable, achievable, results oriented, and time-bound objectives before starting out.</li>
                <li>Continually reviewing the effectiveness, efficiency, equity and legitimacy of your adaptation plans.</li>
                <li>Avoid actions that prevent or limit future adaptation options or restrict the ability of others to adapt.</li>
                <li>Developing a'Theory of Change' can be useful for illustrating the logic of your planning process and checking whether assumptions remain valid.</li>
            </ul>
            <h4><i class="fa fa-book"></i> Resources on Adaptive Project Management</h4>
            <div class="list-group resources">
              <a href="https://ukcip.ouce.ox.ac.uk/wp-content/PDFs/MandE-Guidance-Note3.pdf" class="list-group-item">
                <h4 class="list-group-item-heading">Bours, D., McGinn, C., & Pringle, P. (2014). <em>The Theory of Change approach to climate change adaptation programming.</em> Oxford.</h4>
                <p class="list-group-item-text">https://ukcip.ouce.ox.ac.uk/wp-content/PDFs/MandE-Guidance-Note3.pdf</p>
              </a>
              <a href="https://www.ids.ac.uk/files/dmfile/SilvaVillanueva_2012_Learning-to-ADAPTDP92.pdf" class="list-group-item">
                <h4 class="list-group-item-heading">Villanueva, S. Institute of Development Studies (2010) <em>Learning to ADAPT: Monitoring and Evaluation Approaches in Climate Change Adaptation and Disaster Risk Reduction - Challenges, Gaps and Ways Forward.</em></h4>
                <p class="list-group-item-text">https://www.ids.ac.uk/files/dmfile/SilvaVillanueva_2012_Learning-to-ADAPTDP92.pdf</p>
              </a>
            </div>
        </div>
    </div>
    <div class="modal fade" id="resources-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Resources to Support National Adaptation Plan Development</h4>
          </div>
          <div class="modal-body text-center">
            <div class="list-group resources">
              <a href="http://napglobalnetwork.org/resources/" class="list-group-item">
                <h4 class="list-group-item-heading">International Institute for Sustainable Development (2018) <em>NAP Global Network Resources.</em></h4>
                <p class="list-group-item-text">http://napglobalnetwork.org/resources/</p>
              </a>
              <a href="https://unfccc.int/files/adaptation/application/pdf/nap_booklet.pdf" class="list-group-item">
                <h4 class="list-group-item-heading">UNFCCC, Least Developed Countries Expert Group (2014). The National Adaptation Plan Process: A Brief Overview.</h4>
                <p class="list-group-item-text">https://unfccc.int/files/adaptation/application/pdf/nap_booklet.pdf</p>
              </a>
              <a href="http://www.undp.org/content/undp/en/home/librarypage/climate-and-disaster-resilience-/regional-briefing-on-naps--asia-pacific-in-focus-.html" class="list-group-item">
                <h4 class="list-group-item-heading">UNDP (2018)<em>Regional briefing on National Adaptation Plans (NAPs): Asia-Pacific in focus.</em></h4>
                <p class="list-group-item-text">http://www.undp.org/content/undp/en/home/librarypage/climate-and-disaster-resilience-/regional-briefing-on-naps--asia-pacific-in-focus-.html</p>
              </a>
              <a href="http://napglobalnetwork.org/2017/05/gender-national-adaptation-plan-nap-processes-pilot-review-climate-change-adaptation-planning-documents-15-countries/" class="list-group-item">
                <h4 class="list-group-item-heading">Dazé, A. and Dekens, J. International Institute for Sustainable Development (2012)<em>Gender in National Adaptation Plan (NAP) Processes: A pilot review of climate change adaptation planning documents from 15 countries.</em></h4>
                <p class="list-group-item-text">http://napglobalnetwork.org/2017/05/gender-national-adaptation-plan-nap-processes-pilot-review-climate-change-adaptation-planning-documents-15-countries/</p>
              </a>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</main>

{% include '@apt_theme/partials/footer-partners.html.twig' %}

{% include '@apt_theme/partials/footer.html.twig' %}

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.3.3/vue.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.2/axios.min.js"></script>


