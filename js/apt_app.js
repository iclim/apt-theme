jQuery(function($) {

  /***** VARIABLES *****/

  var drupalToolbarHeight = 39; // Pixels
  var mainNavHeight = 113; // Pixels
  var headerHeight = getHeaderHeight();


  /***** FUNCTION CALLS & EVENT LISTENERS *****/

  /* Change Header Height on Window Resizing */
  jQuery(window).resize(function() {
    headerHeight = getHeaderHeight();
  });

  /* Smooth Scroll Animation */
  // jQuery('body').on('click', 'a[href*=#]:not([href=#])', function(e) {
  jQuery('body').on('click', 'a[href*=\\#]:not([href=\\#])', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - headerHeight }, 500, 'linear');
  });

    $("#tool-stage-options .tool-stage-panel").click(function () {
        // Hide any visible panels and selected marker
        // console.log('BAZ');
        $(".tool-stage-panel").removeClass("selected");
        $(".tool-stage-info-panel").hide();

        // Show the selected marker and selected panel
        $(this).addClass("selected");
        $($(this).find("a").attr("href")).fadeIn();
    });
  

  /* Does the tool welcome modal exist on the page? */
  if ($("#tool-welcome-modal").length) {
    /* Check for cookie to determine if page vistied in past 60 days */
    if (!Cookies.get("visited-tool-page")) {
      $("#tool-welcome-modal").modal("show"); // Launch modal if cookie doesn't exist
      Cookies.set('visited-tool-page', "true", { expires: 60 }); // Set or update cookie
    } else {
      Cookies.remove("visited-tool-page");
      Cookies.set("visited-tool-page", "true", { expires: 60 }); // Set or update cookie
    }

  }

  /* Does the resources-app div exist on the page? */
  if ($("#resources-app").length) {
    addResourceButtonViewEvent();
  }

  /* Is the page a sector guideline page? */
  if ($('body').hasClass('page-node-type-sector-guideline')) {
    var sectorPanelOffset = parseInt($(".sector-panel-wrapper").css("padding-top"));

    /* Sector Menu Scroll Spy */
    $('body').scrollspy({ target: '#spy-sidebar', offset: headerHeight + sectorPanelOffset });

    /* Side Sector Menu Toggle */
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#sector-page").toggleClass("show-menu");
    });

    $("#sector-print-page").click(function(e) {
      e.preventDefault();
      var activeElement = $("#sector-sidebar").find("li.active");

      if (activeElement.has('ul').find('li.active')) {
        activeElement = activeElement.find('li.active');
      }
      var selectors = activeElement.find("a").attr("href");

      $(selectors).printThis({
        header: null,
        footer: null
      });

    });

    $("#sector-download").click(function(e) {
      e.preventDefault();

      // var docTitle = window.href.substr(this.href.lastIndexOf('/') + 1);
      var docTitle = document.title;
      var pdf = new jsPDF('p', 'pt', 'letter');
      // source can be HTML-formatted string, or a reference
      // to an actual DOM element from which the text will be scraped.
      source = $('#sector-page .content')[0];

      // we support special element handlers. Register them with jQuery-style 
      // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
      // There is no support for any other type of selectors 
      // (class, of compound) at this time.
      specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function(element, renderer) {
          // true = "handled elsewhere, bypass text extraction"
          return true
        }
      };
      margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
      };
      // all coords and widths are in jsPDF instance's declared units
      // 'inches' in this case
      pdf.fromHTML(
        source, // HTML string or DOM elem ref.
        margins.left, // x coord
        margins.top, { // y coord
          'width': margins.width, // max width of content on PDF
          'elementHandlers': specialElementHandlers
        },

        function(dispose) {
          // dispose: object with X, Y of the last line add to the PDF 
          //          this allow the insertion of new lines after html
          pdf.save(docTitle + '.pdf');
        }, margins
      );
    });

    fixPanelHeading($('#considerations'), $('#considerations-a'), $('#considerations-d'));
    fixPanelHeading($('#approaches'), $('#approaches-a'), $('#approaches-e'));
    fixPanelHeading($('#studies'), $('#studies-a'), $('#studies-c'));

    $(window).scroll(function() {
      fixPanelHeading($('#considerations'), $('#considerations-a'), $('#considerations-d'));
      fixPanelHeading($('#approaches'), $('#approaches-a'), $('#approaches-e'));
      fixPanelHeading($('#studies'), $('#studies-a'), $('#studies-c'));
    });
  }


  /***** DEFINED FUNCTIONS *****/

  function getHeaderHeight() {
    /* Is the drupal toolbar visible? */
    if ($('body').hasClass('toolbar-fixed')) {
      return drupalToolbarHeight + mainNavHeight;
    } else {
      return mainNavHeight;
    }
  }

  function fixPanelHeading(headingElement, startElement, endElement) {

    var scroll = $(window).scrollTop() + headerHeight;
    var headingFixPos = startElement.offset().top;
    var headingUnfixPos = endElement.offset().top + endElement.outerHeight(true);

    if (scroll >= headingFixPos && scroll < headingUnfixPos && $(this).width() >= 768) {
      headingElement.addClass("fixed-heading");
    } else {
      headingElement.removeClass('fixed-heading');
    }
  }

  function fixToolNavbar() {
    var scroll = $("#tool-stage-navbar-marker").offset().top; // Bottom of screen position
    var toolNavUnfixPos = $("#apt-partners").offset().top;

    if (scroll < toolNavUnfixPos) {
      $("#tool-stage-navbar").css("position", "fixed");
    } else {
      $("#tool-stage-navbar").css("position", "relative");
    }
  }

  function updateNextButton() {
    var currentNavStep = $("#tool-nav-steps").find(".active");

    if($("#tool-nav-steps li").index(currentNavStep) < $("#tool-nav-steps li").length - 1) {
    var nextNavStep = currentNavStep.next("li");
    var nextNavStepTitle = nextNavStep.find(".tool-nav-step-content").text();
    $("#next-step-button").find("span").text(nextNavStepTitle);

    if($("#next-step-button").is(':hidden')) {
        $("#next-step-button").show();
      }
    }
    else {
      $("#next-step-button").hide();
    }
  }

  function addResourceButtonViewEvent() {
    $("#btn-card-view").on("click", function() {
      if($("#resource-cards:visible").length == 0) {
        $("#resource-list").fadeOut();
        $("#resource-cards").fadeIn();
      }
    });

    $("#btn-list-view").on("click", function() {
      if($("#resource-list:visible").length == 0) {
        $("#resource-cards").fadeOut();
        $("#resource-list").fadeIn();
      }
    });
  }

});

if (jQuery('body').hasClass('page-node-type-planning-framework-phase')) {

    Vue.component('stage', {
        data: function () {
            return {
                count: 0
            }
        },

        template: `<div id="stage-wrapper" class="container-fluid">

            <div class="tool-step-content row">
                <stage-start-info v-if="$root.current_step_id == 'start'"></stage-start-info>
                <stage-end-info v-if="$root.current_step_id == 'end'"></stage-end-info>

                <step v-if="$root.current_step_id != 'end' && $root.current_step_id != 'start'" :step="$root.current_step"></step>

            </div>

            <div id="tool-stage-navbar-container" class="row">
                <div id="tool-stage-navbar">
                    <button v-if="$root.current_step_id != 'start'" @click.prevent="$root.goBack" id="stage-back-btn" class="btn btn-primary btn-lg">
                        <i class="fa fa-chevron-left"></i> Back
                    </button>
                    <button v-if="$root.current_step_id != 'end' && $root.current_step_id != 'start'" @click.prevent="$root.goNext" id="stage-next-btn" class="btn btn-primary btn-lg">
                        Next <i class="fa fa-chevron-right"></i>
                    </button>

                    <button v-if="$root.current_step_id == 'start'" @click.prevent="$root.goNext" id="start-stage-btn" class="btn btn-primary btn-lg">
                        Get Started <i class="fa fa-angle-right"></i>
                    </button>
                    <stage-navigator></stage-navigator>
                </div>
             </div>`
                                                
    })
    Vue.component('stage-navigator', {
        data: function () {
            return {
                count: 0
            }
        },
        methods: {
            isSelected(index) {
                return index == this.$root.current_step_id;
            }
        },
        template: `<ul id="tool-nav-steps" class="tool-nav nav">
                        
                        <li><a href="#" @click.prevent="$root.setStepId('start')"><i class="fa fa-home"></i></a></li>
                        <li v-for="step,index in $root.current_stage.steps" v-bind:class="{ 'active' : isSelected(index) }">
                          <a href="#" @click.prevent="$root.setStepId(index)">
                            <span class="tool-nav-step-content">
                            {{step.short_title}}
                            </span>
                          </a>
                        </li>
                        <li><a href="#" @click.prevent="$root.setStepId('end')"><i class="fa fa-file-text"></i></a></li>

                  </ul>`

    })

    Vue.component('stage-start-info', {
        data: function () {
            return {
                count: 0
            }
        },
        

        template: `<div class="">
                <div id="stage-summary" class="col-xs-12 tool-stage-body">
                    <p v-if="$root.current_step_id == 'start'" v-html="$root.current_stage.field_stage_summary"></p>
                </div>

                <div class="col-xs-12" style="min-height: 300px;">
                  <div class="row">
                    <div class="col-sm-4 tool-stage-body">
                        <span v-html="$root.current_stage.field_stage_aim"></span>
                    </div>
                    <div class="col-sm-8 tool-stage-body">
                        <div class="panel panel-default panel-notebook">
                          <div class="panel-body">
                            <span v-html="$root.current_stage.field_stage_output"></span>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>`

    })

    Vue.component('stage-end-info', {
        data: function () {
            return {
                count: 0
            }
        },

        template: `<div class="text-center">
                  <div class="col-xs-12 tool-stage-body">
                    <h2>
                        You're done... Great work!
                    </h2>
                    <div style="min-height: 300px;">
                        
                        <h3>You have worked through our {{$root.current_stage.title}} Notebook and you can now download your documents.</h3>

                        <div class="documents-sticky-note">
                            <table align="center" border="0" cellpadding="1" cellspacing="1">
                              <tbody>
                                <tr>
                                  <td>
                                    <img width="80" src="/themes/APT/images/file-alt-solid.png" alt="Sheet" />                                  </td>
                                  <td>
                                    <img width="80" src="/themes/APT/images/clipboard-check-solid_0.png" alt="Checklist" />
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <p class="text-align-center"><strong>Project Document</strong></p>
                                  </td>
                                  <td>
                                    <p class="text-align-center"><strong>Checklist of actions &amp; key resources</strong></p>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <hr />
                            <button @click.prevent="$root.downloadDOC" class="btn btn-secondary">
                              <i class="fa fa-download"></i> Download Your Documents
                            </button>
                        </div>

                    </div>
                  </div>
              </div>`

    })

    Vue.component('step', {
        props: ['step'],
        data: function () {
            return {
                count: 0
            }
        },

        template: `<div class="">
                <div id="step-title" class="col-xs-12 tool-stage-body">
                     <h2 class="tool-step-title">{{step.short_title}} - {{step.title}}</h2>
                </div>
                <div class="col-xs-12" style="min-height: 300px;">
                  <div class="row">
                    <div class="col-sm-4 tool-stage-body">
                        <span v-html="step.field_step_content"></span>
                    </div>
                    <div class="col-sm-8 tool-stage-body">
                      <div class="panel panel-default panel-notebook">
                        <div class="panel-body">
                          <div v-if="$root.current_step_part == 1">
                            <h3>Tell us more about your project...</h3>
                            <form>
                            
                                <div v-for="userinput in step.step_user_inputs" class="form-group">
  
                                    <label>
                                        {{userinput.label}}
                                    </label>
                                    <textarea v-model="$root.stage_user_input[userinput.code]" class="form-control"></textarea>

                                </div>
                            </form>
                          </div>

                          <div v-if="$root.current_step_part == 2">
                            <h3>Resources</h3>
                            <form>
                                <div v-for="question in step.questions">
                                    <p><strong>{{question.title}}</strong></p>
                                    <div class="radio">
                                      <label class="radio-inline">
                                        <input type="radio" :name="question.question_id" v-model="$root.stage_user_questions[question.question_id]" value="true"> Yes
                                      </label>
                                      <label class="radio-inline">
                                        <input type="radio" :name="question.question_id" v-model="$root.stage_user_questions[question.question_id]" value="false"> No
                                      </label>
                                    </div>
                                </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>
            </div>`

    })


//   Vue.component('stage-resource-selector', {
//     data: function() {
//       return {
//         count: 0
//       }
//     },

//     template: `<div class="">
//               <selectable-resource-link   v-for="resource in this.$root.getCurrentStageResources()" :resource="resource"></selectable-resource-link>
//              </div>`

//   })

//   Vue.component('step-resources', {
//     data: function() {
//       return {
//         count: 0
//       }
//     },
//     template: `<div >
//           <resource-link  v-for="resource in this.$root.getCurrentStepResources()" :resource="resource"></resource-link>
//          </div>`

//   })

  Vue.component('user-email', {
    data: function() {
      return {
        count: 0
      }
    },
    template: `<div>
              <form>
            <div class="input-group input-group-lg" style="z-index: 0;">
                   <input v-model="$root.user_email" style="color:#333;" type="text" class="form-control form-text" type="text" placeholder="Enter your email..." />
                    <span class="input-group-btn">
                      <button @click.prevent="$root.sendEmail" class="btn btn-primary" type="submit"><i class="fa fa-send"></i> Send</button>
                    </span>
                </div>
                <span class="help-block">(NOTE: Your email is private and not shared externally. Read our <a href="#">disclaimer</a> for more information.)</span>
          </form></div>`
  })

//   Vue.component('resource-link', {
//     props: ['resource'],
//     data: function() {
//       return {
//         count: 0
//       }
//     },
//     computed: {
//       linkClass() {
//         var link_class = 'btn-default';
//         if (this.resource.resource_types == 'Template') {
//           link_class = 'btn-primary';
//         }
//         if (this.resource.resource_types == 'Document') {
//           link_class = 'btn-secondary';
//         }
//         if (this.resource.resource_types == 'Web page') {
//           link_class = 'btn-tertiary';
//         }
//         if (this.resource.resource_types == 'Tool') {
//           link_class = 'btn-default';
//         }
//         if (this.resource.resource_types == 'Guideline') {
//           link_class = 'btn-default';
//         }
//         if (this.resource.resource_types == 'Policy') {
//           link_class = 'btn-default';
//         }

//         return link_class;
//       },
//       iconClass() {
//         var link_class = 'fa-link';
//         if (this.resource.resource_types == 'Document') {
//           link_class = 'fa-file-word-o';
//         }
//         if (this.resource.resource_types == 'Guideline') {
//           link_class = 'fa-link';
//         }
//         if (this.resource.resource_types == 'Policy') {
//           link_class = 'fa-link';
//         }
//         if (this.resource.resource_types == 'Template') {
//           link_class = 'fa-file-word-o';
//         }
//         if (this.resource.resource_types == 'Tool') {
//           link_class = 'fa-link';
//         }
//         if (this.resource.resource_types == 'Web page') {
//           link_class = 'fa-external-link';
//         }
//         return link_class;
//       }
//     },
//     template: `<a target="_blank" :href="resource.url" class="btn btn-block btn-resource" v-bind:class="linkClass">
//                 <i class="fa fa-2x pull-left" v-bind:class="iconClass"></i> <i class="fa fa-angle-right fa-2x pull-right"></i> <span class="btn-text">
//               {{resource.title}}<br> 
//               <em>{{resource.resource_types}}</em></span>
//               </a>`
//   })

//   Vue.component('selectable-resource-link', {
//     props: ['resource'],
//     data: function() {
//       return {
//         count: 0
//       }
//     },

//     computed: {
//       isSelected() {
//         return this.$root.isResourceSelected(this.resource.id);
//       },
//       linkClass() {
//         var link_class = 'btn-default';
//         if (this.resource.resource_types == 'Template') {
//           link_class = 'btn-primary';
//         }
//         if (this.resource.resource_types == 'Document') {
//           link_class = 'btn-secondary';
//         }
//         if (this.resource.resource_types == 'Web page') {
//           link_class = 'btn-tertiary';
//         }
//         if (this.resource.resource_types == 'Tool') {
//           link_class = 'btn-default';
//         }
//         if (this.resource.resource_types == 'Guideline') {
//           link_class = 'btn-default';
//         }
//         if (this.resource.resource_types == 'Policy') {
//           link_class = 'btn-default';
//         }

//         return link_class;
//       },
//       iconClass() {
//         var link_class = 'fa-link';
//         if (this.resource.resource_types == 'Document') {
//           link_class = 'fa-file-word-o';
//         }
//         if (this.resource.resource_types == 'Guideline') {
//           link_class = 'fa-link';
//         }
//         if (this.resource.resource_types == 'Policy') {
//           link_class = 'fa-link';
//         }
//         if (this.resource.resource_types == 'Template') {
//           link_class = 'fa-file-word-o';
//         }
//         if (this.resource.resource_types == 'Tool') {
//           link_class = 'fa-link';
//         }
//         if (this.resource.resource_types == 'Web page') {
//           link_class = 'fa-external-link';
//         }
//         return link_class;
//       }
//     },
//     template: `<div class="col-xs-3">
//                 <div class="resource-checkbox "  v-bind:class="{ unchecked: !isSelected }" >
//                 <a @click.prevent="$root.toggleResourceSelection(resource.id)" target="_blank"  class="btn  btn-block btn-resource" v-bind:class="linkClass">
//                 <i class="fa  fa-2x pull-left" v-bind:class="iconClass"></i> 

//                 <i v-if="isSelected" class="fa fa-check fa-2x pull-right"></i>
//                 <i v-if="!isSelected" class="fa fa-plus fa-2x pull-right"></i>

//                 <span class="btn-text">{{resource.title}}<br><em>{{resource.resource_types}}</em></span>
//               </a>
//               </div>
//             </div>`

//   })

//   Vue.component('project-details', {
//     data: function() {
//       return {
//         count: 0
//       }
//     },
//     template: `<form>
//       <div class="tool-step-content row">
//         <div class="tool-step-content-left col-sm-6">
//           <div class="form-group">
//             <label for="project-title">Project Title</label>
//             <input v-model="$root.project_title" type="text" class="form-control" id="project-title">
//           </div>
//           <div class="form-group">
//             <label for="project-descripton">Project Description</label>
//             <textarea v-model="$root.project_description" class="form-control" id="project-descripton" rows="3"></textarea>
//           </div>
//         </div>
//         <div class="tool-step-content-right col-sm-6">
          
//           <div class="form-group">
//             <label for="location">Location or Country of Focus</label>
//             <input v-model="$root.project_location" type="text" class="form-control" id="location">
//           </div>
//           <div class="form-group">
//             <span class="help-block"><em>NOTE: Your project details will be deleted from our system at the end of this planning phase unless you chose to save your progress in the planning stage.</em></span>
//           </div>
//         </div>
//       </div>
//     </form>`
//   })

  apt_app = new Vue({
    el: '#app',
    mounted() {
      // console.log('APT app Initialised');
      this.init();
    },
    delimiters: ['${', '}'],
    data: {
      project_title: 'My Project Title',
      project_description: '',
      project_location: '',
      project_sectors: [],
      user_email: '',
      stage: false,
      selected_resource_ids: [],
      resources: [],
      stages: [],
      current_node_id: false,
      current_step_id: 'start',
      current_step_part: 1,
      current_step: false,
      current_stage: false,
      nav_array: [],
      stage_user_input: {},
      stage_user_questions: {},
      stage_questions: {},
      resources_selected_type: ''
    },

    methods: {
      init() {
        // console.log('init');


        this.current_node_id = window.drupalSettings.path.currentPath.replace('node/', '');
        // console.log('Current Stage is:',this.current_node_id);
        this.getResources();
        var cookie_data = Cookies.getJSON('apt_data');
        // console.log(cookie_data);
        // if (cookie_data && cookie_data.project_title) {
        //   // console.log('load');
        //   this.project_title = cookie_data.project_title;
        //   this.project_description = cookie_data.project_description;
        //   this.project_location = cookie_data.project_location;
        //   this.project_sectors = cookie_data.project_sectors;
        //   this.user_email = cookie_data.user_email;
        //   this.selected_resource_ids = cookie_data.selected_resource_ids;
        // }
        this.nav_array.push('start');


      },
      goNext(){
          var self = this;
          // console.log('goNext', this.current_step_id, this.current_step_part );
          
            if(this.current_step_id == 'start'){
                this.setStepId(0); //go to the first step
            } else {
            
                if (this.current_step_part == 1) {
                    console.log('increase step part');
                    this.current_step_part = 2;
                } else {
                    //Check if it is the last step
                    if (this.current_step_id == this.current_stage.steps.length-1){
                        this.setStepId('end');
                    } else {
                        //not the last step - go to next step
                        this.setStepId(this.current_step_id + 1);
                    }
                    
                }


            }
          
        
      },
      goBack(){
          var self = this;
          // console.log('goBack', this.current_step_id, this.current_step_part);

          if (this.current_step_id == 'end') {
              this.setStepId(this.current_stage.steps.length-1); //go to the last step
          } else {

              if (this.current_step_part == 2) {
                  console.log('decrease step part');
                  this.current_step_part = 1;
              } else {
                  //Check if it is the first step
                  if (this.current_step_id == 0) {
                      this.setStepId('start');
                  } else {
                      //not the last step - go to next step
                      this.setStepId(this.current_step_id - 1);
                  }

              }


          }
      },
      getCurrentStepResources() {
        // console.log('getCurrentStepResources',this.current_step_id);
        if (this.current_step_id) {
          return this.getStepResources(this.current_step_id);
        }
      },
      getStepResources(step_id) {
        var self = this;
        var resources = [];
        if (this.stages[this.current_node_id]) {
          if (this.stages[this.current_node_id]['steps'][this.current_step_id]) {
            var resource_ids = this.stages[this.current_node_id].steps[this.current_step_id].resources;
            resource_ids.forEach(function(id) {
              resources.push(self.getResourceByID(id));
            });
          }
        }
        // console.log('resources to return',resources)
        return resources;
      },
      getCurrentStageResources() {
        var self = this;
        var resources = [];
        var resource_ids = [];
        if (this.stages[this.current_node_id]) {
          for (step in this.stages[this.current_node_id]['steps']) {
            this.stages[this.current_node_id]['steps'][step].resources.forEach(function(id) {
              // console.log('trying to add',id,resource_ids.indexOf(id));
              if (resource_ids.indexOf(id) == -1) {
                // console.log('its not in the list', id);
                resources.push(self.getResourceByID(id));
              } else {
                // console.log('its already in the list',id);
              }
              resource_ids.push(id);
            });
          }
        }
        // console.log('resources to return',resources)
        return resources;
      },
      selectAllStageResources() {
        // console.log('selectAllStageResources');
        var self = this;

        self.getCurrentStageResources().forEach(function(resource) {
          self.selected_resource_ids.push(resource.id);
        });
        // console.log('selectAllStageResources',self.selected_resource_ids)
      },
      isResourceSelected(id) {
        return this.selected_resource_ids.indexOf(id) > -1;
      },
      toggleResourceSelection(id) {
        // console.log('toggleResourceSelection',id);
        var selected_index = this.selected_resource_ids.indexOf(id);
        if (selected_index > -1) {
          //resource is selected
          //now we deselect it
          this.selected_resource_ids.splice(selected_index, 1);
        } else {
          // add it
          this.selected_resource_ids.push(id);
        }
        // console.log('after toggle',this.selected_resource_ids);
      },
      getResourceByID(id) {
        return this.resources[id];
      },
      setStepId(id) {
        console.log('Set Step Id',id);
        this.current_step_id = id;
        if(id != 'start' || id != 'end'){
            this.current_step = this.current_stage.steps[this.current_step_id];
            this.current_step_part = 1; //always set to 1 when changing step
            // console.log(this.current_step);
        } else {
            this.current_step = false;
        }
      },
      getResources: function() {
        // console.log('Load resources')
        var self = this;
        // axios.get('https://pacificclimatechange.net/document-search-json?f[0]=field_tags:3142', {
        axios.get('/resources.php', {

          })
          .then(function(response) {
            // console.log(response.data);
            if (response.data) {
              self.resources = response.data;
              self.getStages();
            }

          })
          .catch(function(error) {
            console.log(error);
          });
      },
      getStages: function() {
        console.log('Load Stages')
        var self = this;
        // axios.get('https://pacificclimatechange.net/document-search-json?f[0]=field_tags:3142', {
        axios.get('/stages.php', {

          })
          .then(function(response) {
            console.log(response.data);
            if (response.data) {
              self.stages = response.data;
              if(self.stages[self.current_node_id]){
                  self.current_stage = self.stages[self.current_node_id];
                //   console.log('current stage', self.current_stage);
                  self.current_stage['steps'].forEach(function (step,index) {
                      self.nav_array.push(index);
                      //add questions and inputs
                      step['questions'].forEach(function (question, index) {
                          self.stage_user_questions[question.question_id] = false; //default value

                          self.stage_questions[question.question_id] = question; 
                      });

                      step['step_user_inputs'].forEach(function (userinput, index) {
                          self.stage_user_input[userinput.code] = ''; //default value
                      });
                  });
                  self.nav_array.push('end');
                //   console.log(self.nav_array);
              }
              console.log('current stage', self.current_stage, self.stage_user_input, self.stage_user_questions  );
            
            //   self.selectAllStageResources();
            }

          })
          .catch(function(error) {
            console.log(error);
          });
      },
      saveProgress() {
        this.saveData('Your progress has been saved.');
      },
      saveData(alertuser = false) {
        var self = this;
        var cookie_data = {};
        cookie_data.project_title = this.project_title;
        cookie_data.project_description = this.project_description;
        cookie_data.project_location = this.project_location;
        cookie_data.project_sectors = this.project_sectors;
        cookie_data.user_email = this.user_email;
        cookie_data.selected_resource_ids = this.selected_resource_ids;
        Cookies.set('apt_data', cookie_data);
        // console.log('cookie set',cookie_data);
        if (alertuser) {
          // alert(alertuser);
          swal(
            'Information',
            alertuser,
            'info'
          );
        }

      },
      clearData() {
        this.project_title = 'My Project Title',
          this.project_description = '',
          this.project_location = '',
          this.project_sectors = [],
          this.user_email = '',
          this.stage = false,
          this.selected_resource_ids = [];



        // this.saveData('Data has been cleared.');

        // Cookies.remove('apt_data');
        // alert('Data has been cleared.');
        swal(
          'Information',
          'Your data has been cleared.',
          'info'
        );



      },
      downloadPDF() {
        // console.log('downloadPDF');
      },
      downloadDOC() {
        // console.log('downloadDOC');
        var self = this;
        var params = new URLSearchParams();
        
        params.append('stage_id', self.current_node_id);

        for (var key in self.stage_user_input) {
              params.append(key, self.stage_user_input[key]);
        }

        

        var selected_question_ids = [];
        var selected_resource_ids = [];
        for (var key in self.stage_user_questions) {
            if (self.stage_user_questions[key]){
                // console.log('selected question', key, self.stage_user_questions[key]);
                selected_question_ids.push(key);
                if(self.stage_questions[key]){
                    // console.log(self.stage_questions[key].related_resource_ids);
                    selected_resource_ids = selected_resource_ids.concat(self.stage_questions[key].related_resource_ids);
                }
            }
            
        }

          params.append('resource_ids',selected_resource_ids);
          params.append('question_ids', selected_question_ids);

        //   console.log('selected resource ids', selected_resource_ids, selected_question_ids);



    

        
          
          if (self.current_stage['stage-code'] == 'stage-1') {

              axios.post('/generateProjectSheetDocument.php', params)
                  .then(function (response) {
                      // console.log(response.data);
                      if (response.data) {
                          //do somethibg
                          var contentDocument = response.data;
                          var content = '<!DOCTYPE html>' + contentDocument;
                          // var orientation = document.querySelector('.page-orientation input:checked').value;
                          var converted = htmlDocx.asBlob(content);
                          saveAs(converted, 'Project Concept Note.docx');

                      }

                  })
                  .catch(function (error) {
                      console.log(error);
                  });

          } else {
              axios.post('/generateProjectSheetDocument2.php', params)
                  .then(function (response) {
                      // console.log(response.data);
                      if (response.data) {
                          //do somethibg
                          var contentDocument = response.data;
                          var content = '<!DOCTYPE html>' + contentDocument;
                          // var orientation = document.querySelector('.page-orientation input:checked').value;
                          var converted = htmlDocx.asBlob(content);
                          saveAs(converted, 'Project Proposal Note.docx');

                      }

                  })
                  .catch(function (error) {
                      console.log(error);
                  });

          }
      

        

         

          axios.post('/generateStageDocument.php', params)
          .then(function(response) {
            // console.log(response.data);
            if (response.data) {
              //do somethibg
              var contentDocument = response.data;
              var content = '<!DOCTYPE html>' + contentDocument;
              // var orientation = document.querySelector('.page-orientation input:checked').value;
              var converted = htmlDocx.asBlob(content,{orientation: 'landscape'});
              saveAs(converted, 'Checklist and Resources.docx');
            }

          })
          .catch(function(error) {
            console.log(error);
          });

      },
      sendEmail() {
        // console.log('sendEmail');
        swal(
          'Great!',
          'An email containing the generated document will now be sent to your email!',
          'success'
        );
      }
    }
  });

}

if(document.getElementById("resources-app")) {

  //Vue script here
  var resources_app = new Vue({
    el: '#resources-app',
    delimiters: ['${', '}'],
    data: {
      message: 'Hello Vue!',
      resources: [],
      show_templates: true,
      show_guidelines: true,
      show_policies: true,
      show_tools: true,
      show_casestudies: true,
    },
    mounted: function() {
      this.getData();
    },
    filters: {
      classname: function(value) {
        if (!value) return ''
        value = value.toString()
        return value.replace(/\s+/g, '-').toLowerCase()
      },
      truncate: function(string) {
        // console.log('truncate: '+string.length);
        var len = 100;
        if (string.length > len) {
          return string.substring(0, len) + '...';
        }
        return string;
      }
    },
    methods: {
      getData: function() {
        // console.log('Load resources')
        var self = this;
        // axios.get('https://pacificclimatechange.net/document-search-json?f[0]=field_tags:3142', {
        axios.get('/resources.php', {
            //
          })
          .then(function(response) {
            // console.log(response.data);
            self.resources = response.data;

          })
          .catch(function(error) {
            console.log(error);
          });
      }
    }

  })
}
